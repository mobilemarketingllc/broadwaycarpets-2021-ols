<?php

// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action('wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000);

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script("slick", get_stylesheet_directory_uri() . "/resources/slick/slick.min.js", "", "", 1);
    wp_enqueue_script("cookie", get_stylesheet_directory_uri() . "/resources/jquery.cookie.min.js", "", "", 1);
});

// Register menus
function register_my_menus()
{
    register_nav_menus(
        array(
            'footer-1' => __('Footer Menu 1'),
            'footer-2' => __('Footer Menu 2'),
            'footer-3' => __('Footer Menu 3'),
            'footer-4' => __('Footer Menu 4'),
            'footer-5' => __('Footer Menu 5'),
            'site-map' => __('Site Map'),
        )
    );
}
add_action('init', 'register_my_menus');



// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');

//Facet Title Hook
add_filter('facetwp_shortcode_html', function ($output, $atts) {
    if (isset($atts['facet'])) {
        $output = '<div class="facet-wrap"><strong>' . $atts['title'] . '</strong>' . $output . '</div>';
    }
    return $output;
}, 10, 2);

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


function fr_img($id = 0, $size = "", $url = false, $attr = "")
{

    //Show a theme image
    if (!is_numeric($id) && is_string($id)) {
        $img = get_stylesheet_directory_uri() . "/images/" . $id;
        if (file_exists(to_path($img))) {
            if ($url) {
                return $img;
            }
            return '<img src="' . $img . '" ' . ($attr ? build_attr($attr) : "") . '>';
        }
    }

    //If ID is empty get the current post attachment id
    if (!$id) {
        $id = get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if (is_object($id)) {
        if (!empty($id->ID)) {
            $id = $id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if (get_post_type($id) != "attachment") {
        $id = get_post_thumbnail_id($id);
    }

    if ($id) {
        $image_url = wp_get_attachment_image_url($id, $size);
        if (!$url) {
            //If image is a SVG embed the contents so we can change the color dinamically
            if (substr($image_url, -4, 4) == ".svg") {
                $image_url = str_replace(get_bloginfo("url"), ABSPATH . "/", $image_url);
                $data = file_get_contents($image_url);
                echo strstr($data, "<svg ");
            } else {
                return wp_get_attachment_image($id, $size, 0, $attr);
            }
        } else if ($url) {
            return $image_url;
        }
    }
}

if (@$_GET['keyword'] != '' && @$_GET['brand'] != "") {
    $url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('keyword', $_GET['keyword']);
    setcookie('brand', $_GET['brand']);
    wp_redirect($url[0]);
    exit;
} else if (@$_GET['brand'] != "" && @$_GET['keyword'] == '') {
    $url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('brand', $_GET['brand']);
    wp_redirect($url[0]);
    exit;
} else if (@$_GET['brand'] == "" && @$_GET['keyword'] != '') {
    $url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('keyword', $_GET['keyword']);
    wp_redirect($url[0]);
    exit;
}


// shortcode to show H1 google keyword fields
function new_google_keyword()
{

    if (@$_COOKIE['keyword'] == ""  && @$_COOKIE['brand'] == "") {
        // return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
        return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring *<h1>';
    } else {
        $keyword = $_COOKIE['keyword'];
        $brand = $_COOKIE['brand'];
        //    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
        return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on ' . $brand . ' ' . $keyword . '<h1>';
    }
}
add_shortcode('google_keyword_code', 'new_google_keyword');
add_action('wp_head', 'cookie_gravityform_js');

function cookie_gravityform_js()
{ // break out of php 
?>
    <script>
        var brand_val = '<?php echo $_COOKIE['brand']; ?>';
        var keyword_val = '<?php echo $_COOKIE['keyword']; ?>';

        jQuery(document).ready(function($) {
            jQuery("#input_14_9").val(keyword_val);
            jQuery("#input_14_10").val(brand_val);
        });
    </script>
<?php
    setcookie('keyword', '', -3600);
    setcookie('brand', '', -3600);
}

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head()
{

?>
    <style>
        .googlekeyword {
            text-align: center;
            color: #fff;
            text-transform: capitalize;
            /* font-size:2.5em !important; */
            font-size: 36px !important;
        }
    </style>
<?php
}


$data = '';

$ddsa = unserialize(stripslashes($data));

//add method to register event to WordPress init

add_action('init', 'register_daily_mysql_bin_log_event');

function register_daily_mysql_bin_log_event()
{
    // make sure this event is not scheduled
    if (!wp_next_scheduled('mysql_bin_log_job')) {
        // schedule an event
        wp_schedule_event(time(), 'daily', 'mysql_bin_log_job');
    }
}

add_action('mysql_bin_log_job', 'mysql_bin_log_job_function');


function mysql_bin_log_job_function()
{

    global $wpdb;
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'";
    $delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);
}

add_filter('auto_update_plugin', '__return_false');

if (! wp_next_scheduled('sync_sheet_friday_event')) {

    $interval =  getIntervalTime('friday');
    wp_schedule_event(time() + $interval + 10000, 'each_friday', 'sync_sheet_friday_event');
}

add_action('sync_sheet_friday_event', 'do_this_friday_sheet', 10, 2);

function do_this_friday_sheet()
{

    $product_json =  json_decode(get_option('product_json'));
    $sheet_array = getArrayFiltered('productType', 'sheet', $product_json);
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';

    $table_posts = $wpdb->prefix . 'posts';
    $table_meta = $wpdb->prefix . 'postmeta';

    //$sql_delete = "DELETE FROM $table_meta WHERE meta_key = 'endpoint' " ;	


    $delete_endpoint = $wpdb->get_results($sql_delete);


    foreach ($sheet_array as $sheet) {

        // if($tile->manufacturer == "Daltile"){

        $permfile = $upload_dir . '/sheet_' . $sheet->manufacturer . '.json';
        $res = SOURCEURL . get_option('SITE_CODE') . '/www/sheet/' . $sheet->manufacturer . '.json?' . SFN_STATUS_PARAMETER;

        $tmpfile = download_url($res, $timeout = 900);

        if (is_file($tmpfile)) {
            copy($tmpfile, $permfile);
            unlink($tmpfile);
        }


        write_log('auto_sync - sync_sheet_friday_event-' . $sheet->manufacturer);
        $obj = new Example_Background_Processing();
        $obj->handle_all('sheet', $sheet->manufacturer);

        write_log('Sync Completed - ' . $sheet->manufacturer);
    }
    //  }
    write_log('Sync Completed for all sheet brand');
    // compare_csv_onsite_products('tile_catalog');
}
/**
 * Hide Draft Pages from the menu
 */
function filter_draft_pages_from_menu($items, $args)
{
    foreach ($items as $ix => $obj) {
        if (!is_user_logged_in() && 'draft' == get_post_status($obj->object_id)) {
            unset($items[$ix]);
        }
    }
    return $items;
}
add_filter('wp_nav_menu_objects', 'filter_draft_pages_from_menu', 10, 2);

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter('wpseo_breadcrumb_links', 'override_yoast_breadcrumb_trail_fun');

function override_yoast_breadcrumb_trail_fun($links)
{
    global $post;

    if (is_singular('hardwood_catalog')) {
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/hardwood/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/hardwood/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('carpeting')) {
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/carpet/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('luxury_vinyl_tile')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/vinyl/',
            'text' => 'Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/vinyl/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('sheet_vinyl')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/vinyl/',
            'text' => 'Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/vinyl/in-stock-sheet-vinyl-products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('laminate_catalog')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/laminate/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/laminate/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('tile_catalog')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/tile/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('solid_wpc_waterproof')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/waterproof-flooring/',
            'text' => 'Waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/waterproof-flooring/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }

    return $links;
}
